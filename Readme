Technology Choice
=================

### run the project -> `make install` then `make love`

- `make install` will install Poetry and all it's deps
- `make love` will run the Python server
- `make pytest` will run the Python test
- `make jsdev` will start watch and build the frontend part of this project(not required for running), you need run `npm install` by yourself.

Python Server:
------

"The server should act as an intermediate cache and as a transformation layer on top of
public Aiven REST API."

Here I built a simple GraphQL wrapper of the existing REST API, visit http://localhost:5000/graphql for more details.

Key Words: Flask + graphene

ReactJS Client:
---------------
Frontend: Parcel + TypeScript + React

No redux/apollo, only hooks and raw graphql call, tests are running in browser with mocha.
To simplify the running process, I commit the build results to the Repo, so `npm install` is not required to run this project. 

---------------------------------------------------------------------------------------------------

Aiven homework
==============

This is a coding assignment for a Full-Stack developer position at Aiven.

The exercise should be relatively fast to complete. You can spend as much time
as you want to. If all this is very routine stuff for you, this should not take
more than a few hours. If there are many new things, a few evenings should
already be enough time.

This is one thing we use when selecting the candidates for the interview, so please pay
attention that your solution demonstrates your skills in developing production
quality code.

If you run out of time, please return a partial solution, and describe in your
reply how you would continue having more time.

Please use React.js and Python for the exercise, otherwise, you have the freedom to select
suitable tools and libraries.

Be prepared to defend your solution in the possible interview later.

To return your homework, store the code and related documentation on GitHub
for easy access. Please send following information via email:

- link to the GitHub repository
- if you ran out of time and you are returning a partial solution, description
  of what is missing and how you would continue

Your code will only be used for the evaluation.


Exercise
========

Aiven is a Database As a Service provider. A customer can launch a database in
any of the supported clouds (like Google or Amazon cloud) using the Aiven web
console.

Your task is to create a prototype web application for improved cloud selection
logic. The application should include a frontend and a server implementation.
The server should act as an intermediate cache and as a transformation layer on top of
public Aiven REST API.

Aiven's clouds can be listed using the API as described here
https://api.aiven.io/doc/#api-Cloud-ListClouds.

It should be possible to select and filter regions by the cloud provider
(e.g. Amazon Web Services or Google Cloud Platform) and by distance (shortest
distance to the user). The distance comparison should be based on
Geolocation. Our API returns latitude and longitude values for each region.


Criteria for evaluation
=======================
- Code formatting and clarity. We value readable code written for other
  developers, not for a tutorial, or as one-off hack.
- Practicality of testing. 100% test coverage may not be practical, and also
  having 100% coverage but no validation is not very useful.
- Automation. We like having things work automatically, instead of multi-step
  instructions to run misc commands to set up things. Similarly, CI is a
  relevant thing for automation.
- Attribution. If you take code from Google results, examples etc., add
  attributions. We all know new things are often written based on search
  results.
- "Open source ready" repository. It's very often a good idea to pretend the
  homework assignment in Github is used by random people (in practice, if you
  want to, you can delete/hide the repository as soon as we have seen it).
