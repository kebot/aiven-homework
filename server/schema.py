from graphene import ObjectType, String, Float, Schema, List
import requests
from functools import lru_cache
from geopy import distance

# This will only fetch cloud api once...
@lru_cache(32)
def fetch_clouds():
    r = requests.get('https://api.aiven.io/v1/clouds')
    return r.json()

class CloudType(ObjectType):
    cloud_description = String()
    cloud_name = String()  # "azure-south-africa-north",
    geo_latitude = Float()  # -26.198,
    geo_longitude = Float() # -# 28.03,
    geo_region = String()  # "africa"

class Query(ObjectType):
    # this defines a Field `hello` in our Schema with a single Argument `name`
    author=String(name = String(default_value="stranger"))
    clouds=List(CloudType, latitude = Float(default_value=52.0), longitude = Float(default_value=13.0),)

    def resolve_author(root, info, name):
        return "Keith Yao"

    def resolve_clouds(root, info, latitude, longitude):
        r = fetch_clouds()
        clouds = r['clouds']

        # Calculate the distance between the server and current location by using GeoPy
        return sorted(clouds, key=lambda cloud: distance.distance(
            (cloud["geo_latitude"], cloud["geo_longitude"]),
            (latitude, longitude)
        ).km)

schema = Schema(query = Query)
