__version__ = '0.1.0'

from flask import Flask, send_from_directory
from flask_graphql import GraphQLView
from graphql import GraphQLCachedBackend
from .schema import schema

def create_app(path='/graphql', **kwargs):
    backend = None
    app = Flask(__name__)

    app.debug = True
    app.add_url_rule(path, view_func=GraphQLView.as_view('graphql', schema=schema, backend=backend, **kwargs))


    @app.route('/')
    def home():
        return send_from_directory('../dist', 'index.html', cache_timeout=0, add_etags=False)

    @app.route('/<path:filename>')
    def serve_dist(filename):
        return send_from_directory('../dist', filename)

    return app
