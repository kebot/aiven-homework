import 'mocha'
import * as assert from 'assert'

mocha.setup('bdd')

import distance from './geo-distance'

describe('geo-distance', () => {
  it('distance between Berlin and Helsinki should be larger than 100', () => {
    assert.equal(distance(52, 13, 58, 24) > 100, true)
  })
})

import { fetchAllClouds } from './useCloudSelector'

describe('Graphql API', () => {
  it('should able to fetch the api', async () => {
    const { clouds } = await fetchAllClouds ()

    assert.equal(!!clouds[0].cloudName, true)
    assert.equal(!!clouds[0].cloudDescription, true)
    assert.equal(!!clouds[0].geoLatitude, true)
    assert.equal(!!clouds[0].geoLongitude, true)
    assert.equal(!!clouds[0].geoRegion, true)
  })
})

mocha.run()
