// Cool , let's write some TypeScript
import '@blueprintjs/core/lib/css/blueprint.css'
import './app.css'

import * as React from 'react'
import { render } from 'react-dom'

import {
  Card,
  Callout,
  AnchorButton,
  Button,
  Navbar,
  Alignment,
  Tabs,
  Tab,
  Elevation,
  Intent,
  Switch,
  H1,
  H5,
  Tag,
} from '@blueprintjs/core'

import sortBy from 'lodash/sortBy'

import useCloudSelector, { Cloud } from './useCloudSelector'

const ServerListPanel: React.FC<{
  name: string
  cloud: Cloud[]
  selectedCloud: string
  setSelectedCloud: (string) => void
  byDistance: boolean
}> = ({ clouds, selectedCloud, setSelectedCloud, byDistance }) => {
  const sortedClouds = React.useMemo(() => {
    return byDistance
      ? sortBy(clouds, 'distance')
      : sortBy(clouds, (cloud: Cloud) => cloud.geoRegion)
  }, [byDistance, clouds])

  return (
    <div>
      {sortedClouds.map((cloud: Cloud) => {
        return (
          <div className='list-item-container' key={cloud.cloudName}>
            <Card
              interactive
              elevation={selectedCloud === cloud.cloudName ? Elevation.FOUR : Elevation.ONE}
              onClick={() => setSelectedCloud(cloud.cloudName)}
            >
              <H5 className='bp3-heading'>{cloud.cloudName}</H5>
              <p>
                {cloud.distance && <Tag>{cloud.distance.toFixed()} KM</Tag>} &nbsp;{' '}
                {cloud.cloudDescription}
              </p>
            </Card>
          </div>
        )
      })}
    </div>
  )
}

const App: React.FC<{}> = () => {
  const byFilter = useCloudSelector()
  const [selectedCloud, setSelectedCloud] = React.useState<string>()
  const [byDistance, setByDistance] = React.useState<boolean>(false)

  return (
    <div className='container bp3-dark'>
      <Navbar>
        <Navbar.Group align={Alignment.LEFT}>
          <Navbar.Heading>Aiven Homework</Navbar.Heading>
          <Navbar.Divider />
          <Button className='bp3-minimal' icon='home' text='Home' />
        </Navbar.Group>

        <Navbar.Group align={Alignment.RIGHT}>
          <AnchorButton text='Run Mocha test' href='/test.html' target='_blank' icon='lab-test' />
          <Navbar.Divider />
          <AnchorButton text='Graphql Terminal' href='/graphql' target='_blank' icon='tree' />
        </Navbar.Group>
      </Navbar>

      <H1 className='hp3-heading'>Select Service Cloud Provider</H1>
      <p>It should be able to filter regions "by the cloud provider" and "by distance"</p>

      {selectedCloud && (
        <Callout title={`You choosed ${selectedCloud}`} intent={Intent.SUCCESS}></Callout>
      )}

      <Tabs
        large
        id='serverGroup'
        onChange={() => console.log('group selection change')}
        defaultSelectedTabId={'azure'}
      >
        {Object.keys(byFilter).map((key) => (
          <Tab
            key={key}
            id={key}
            title={key}
            panel={
              <ServerListPanel
                clouds={byFilter[key]}
                name={key}
                selectedCloud={selectedCloud}
                setSelectedCloud={setSelectedCloud}
                byDistance={byDistance}
              />
            }
          />
        ))}
        <Tabs.Expander />
        <Switch
          checked={byDistance}
          onChange={() => setByDistance(!byDistance)}
          label='Sort by distance'
        />
      </Tabs>
    </div>
  )
}

render(<App />, document.getElementById('app'))
