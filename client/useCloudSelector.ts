import * as React from 'react'
import { useGeolocation } from 'react-use'
import distance from './geo-distance'

export interface Cloud {
  cloudDescription: string
  cloudName: string
  geoLatitude: number
  geoLongitude: number
  geoRegion: string
  distance?: number
}

export async function fetchAllClouds(): Promise<{ clouds: Cloud[] }> {
  const r = await fetch('/graphql?', {
    headers: { accept: 'application/json', 'content-type': 'application/json' },
    body:
      '{"query":"{\\n  clouds {\\n    cloudDescription\\n    cloudName\\n    geoLatitude\\n    geoLongitude\\n    geoRegion\\n  }\\n}\\n","variables":null,"operationName":null}',
    method: 'POST',
    mode: 'cors',
  })
  const { data } = await r.json()

  return data
}

export default function useCloudSelector(): { [filter: string]: Cloud[] } {
  const [clouds, setClouds] = React.useState<Cloud[]>([])
  const myLocation = useGeolocation()

  React.useEffect(() => {
    let isUnmounted = false

    ;(async () => {
      // here uses raw Graphql Query
      const { clouds } = await fetchAllClouds()

      if (isUnmounted) {
        return
      }

      setClouds(
        myLocation && !myLocation.loading
          ? clouds.map((cloud) => ({
              ...cloud,
              distance: distance(
                cloud.geoLatitude,
                cloud.geoLongitude,
                myLocation.latitude,
                myLocation.longitude
              ),
            }))
          : clouds
      )
    })()

    return () => (isUnmounted = true)
  }, [myLocation])

  let byFilter = React.useMemo(
    () =>
      clouds.reduce((prevValue, curr) => {
        const key = curr.cloudName.split('-')[0]

        if (prevValue[key]) {
          prevValue[key].push(curr)
        } else {
          prevValue[key] = [curr]
        }

        return prevValue
      }, {}),
    [clouds]
  )

  return byFilter
}
