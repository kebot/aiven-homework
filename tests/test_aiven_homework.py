from server.schema import schema
from graphene.test import Client

def test_version():
    client = Client(schema)
    results = client.execute('''{ clouds { cloudName } }''')
    assert type(results['data']['clouds'][0]['cloudName']) == str
