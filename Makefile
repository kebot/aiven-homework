love:
	@echo "Starting Dev Server"
	poetry run python dev.py

install:
	# pre-requirement: python3
	pip3 install poetry
	poetry install

pytest:
	poetry run python -m pytest

jsdev:
	npx parcel watch client/index.html

.PHONY: love, install
